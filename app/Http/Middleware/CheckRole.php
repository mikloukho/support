<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    public function handle(Request $request, Closure $next, array ...$roles): Closure
    {
        if(Auth::user()?->hasRole($roles)) {
            return $next($request);
        }
       abort(403);
    }
}

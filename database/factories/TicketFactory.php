<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ticket>
 */
class TicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title'  => $this->faker->sentence(),
            'description'   => $this->faker->sentence(),
            'status' => Ticket::STATUSES[array_rand(Ticket::STATUSES)],
            'user_id' => User::factory(),
        ];
    }
}

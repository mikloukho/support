<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
        ]);
        User::factory(1)->admin()->create();
        User::factory(5)->client()->has(Ticket::factory(3))->create();
    }
}

<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Role::ROLES as $name => $id) {
            Role::updateOrCreate(
                ['id'   => $id],
                ['name' => $name]
            );
        }
    }
}

<?php

namespace Tests\Unit;


use Tests\TestCase;
use App\Models\User;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    use RefreshDatabase;

    public function dataRoleProvider(): array
    {
        return [
            [
                Role::ROLES['Admin'],
                Role::ROLES['Admin'],
                true
            ],
            [
                Role::ROLES['Manager'],
                [Role::ROLES['Manager'], Role::ROLES['Client']],
                true
            ],
            [
                Role::ROLES['Client'],
                [Role::ROLES['Manager'], Role::ROLES['Main-Manager']],
                false
            ],
        ];
    }

    /**
     * @dataProvider dataRoleProvider
     * @param int $userRole
     * @param int|array $testRoles
     * @param bool $exceptedResult
     * @return void
     */
    public function testHasRole(int $userRole, int|array $testRoles, bool $exceptedResult): void
    {
        $user = User::factory(['role_id' => $userRole])->create();
        $result = $user->hasRole($testRoles);
        $this->assertEquals($exceptedResult, $result);
    }
}
